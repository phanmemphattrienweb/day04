<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Đăng ký Tân sinh viên</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css"
        rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js">
    </script>

    <style>
    .signup-background {
        width: 40rem;
        border: solid 2px #4e7aa3;
        margin: auto;
        margin-top: 2rem;
        padding: 0.6rem 0.8rem;
    }

    .signup {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        padding: 2rem 2.8rem;
    }

    .signup-form {
        font-size: 16px;
        width: 100%;
        display: flex;

    }

    .signup-form-text {
        background-color: #5b9bd5;
        border: 2px solid #4e7aa3;
        width: 8rem;
        padding: 0.6rem 0.6rem 0.2rem;
        margin-right: 2rem;
        text-align: center;
        color: #fff;
    }

    .input-text {
        width: 24rem;
        height: 2.6rem;
        padding-left: 0.4rem;
        border: 2px solid #4e7aa3;
    }

    .gender {
        display: flex;
        align-items: center;
        width: 50px;
        padding: 0px;
        margin-left: 10px;
    }

    select {
        border: 2px solid #4e7aa3;
        padding: 0px;
        outline: none;
        width: 30%;
        height: 2.6rem;
    }

    .input-birthday {
        width: 30%;
        height: 2.6rem;
        padding-left: 0.4rem;
        border: 2px solid #4e7aa3;
    }

    .signup-submit {
        display: flex;
        justify-content: center;
        margin-top: 2rem;
    }

    input[type="submit"] {
        height: 2.8rem;
        width: 8rem;
        background-color: #70ad46;
        border-radius: 10px;
        border: solid 2px #477990;
        cursor: pointer;
        color: white;
    }
    </style>
</head>

<body>


    <fieldset class="signup-background">

        <?php
    
        $fullName = $gender = $department = $birthday = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if(empty(checkInput($_POST["fullName"]))){
                echo "<div style='color: red;'>Hãy nhập tên.</div>";
            }
            if (empty($_POST["gender"])) {
                echo "<div style='color: red;'>Hãy chọn giới tính.</div>";
            }
            if(empty(checkInput($_POST["department"]))){
                echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
            }

            if(empty(checkInput($_POST["birthday"]))){
                echo "<div style='color: red;'>Hãy nhập ngày sinh.</div>";
            }
            elseif (!validateDate($_POST["birthday"])) {
                echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng.</div>";
            }
    
        }

        function checkInput($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }

        function validateDate($date, $format = 'd/m/Y')
        {
            $d = DateTime::createFromFormat($format, $date);
            return $d && $d->format($format) == $date;
        }
    ?>
        <div class="signup">

            <form action="" method="POST">
                <div class="signup-form">
                    <p class="signup-form-text">
                        Họ và tên
                        <span style="color: red">*</span>
                    </p>

                    <input name="fullName" type="text" class="input-text">
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Giới tính
                        <span style="color: red">*</span>
                    </p>

                    <div class="gender">
                        <?php
                                $gender = array('0' => 'Nam', '1' => 'Nữ');
                                for ($i = 0; $i < count($gender); $i++) {
                                    echo '
                                        <input type="radio" id="' . $i . '" name="gender" value="' . $gender[$i] . '">
                                    ';
                                    echo '
                                        <label for="' . $i . '" style="margin: 0 20px 4px 6px">' . $gender[$i] . '</label> 
                                    ';
                                }
                            ?>
                    </div>

                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Phân khoa
                        <span style="color: red">*</span>
                    </p>

                    <select name='department'>
                        <?php 
                            $depart = array("EMPTY"=>"", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                            foreach ($depart as $key => $value) {
                                echo '<option >' . $value . '</option>';
                              }
                              ?>
                    </select>
                </div>
                <div class="signup-form">
                    <p class="signup-form-text">
                        Ngày sinh
                        <span style="color: red">*</span>
                    </p>
                    <input type="text" name="birthday" id="birthday" class="input-birthday" placeholder="dd/mm/yyyy">
                    <script type="text/javascript">
                    $(".birthday").datepicker({
                        format: "dd/mm/yyyy",
                    });
                    </script>

                </div>

                <div class="signup-form">
                    <p class="signup-form-text">
                        Địa chỉ
                    </p>
                    <input type="text" name="address" id="address" class="input-text">


                </div>
                <div class="signup-form signup-submit">
                    <input type="submit" value="Đăng Kí">
                </div>
            </form>
        </div>

    </fieldset>


</body>

</html>